#!/bin/bash
set -e

#http://stackoverflow.com/questions/2497215/extract-domain-name-from-url
ROS_MASTER_HOSTNAME=$(echo $ROS_MASTER_URI | sed -e "s/[^/]*\/\/\([^@]*@\)\?\([^:/]*\).*/\2/")
echo "Hostname '$ROS_MASTER_HOSTNAME' extracted from ROS_MASTER_URI=$ROS_MASTER_URI"

# Check for ntpdate
if ! [ $(which ntpdate) ]; then
  echo "Installing ntpdate..."
  echo -e "Executing \033[1msudo apt-get install ntpdate\033[0m"
  sudo apt-get install ntpdate
fi

echo "Querying time server..."
ntpdate -q $ROS_MASTER_HOSTNAME

if [ $(which chronyd) ]; then
echo "Stopping chrony..."
sudo service chrony stop
fi

echo "Syncing with time server..."
sudo ntpdate $ROS_MASTER_HOSTNAME

if [ $(which chronyd) ]; then
echo "Starting chrony..."
sudo service chrony start
fi

echo "Querying time server..."
sleep 2
ntpdate -q $ROS_MASTER_HOSTNAME

