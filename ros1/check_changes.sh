#!/bin/sh

grep local-name: src/.rosinstall | awk '{ print $2 }' |
while read repo; do
	printf "%s\n" "${repo}"
	git -C src/${repo} status -sb
	printf "\n"
done 
