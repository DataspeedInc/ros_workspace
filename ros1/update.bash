#!/bin/bash
set -e

# Check for wstool
if ! [ $(which wstool) ]; then
  echo "Installing wstool..."
  echo -e "Executing \033[1msudo apt-get install python-wstool\033[0m"
  sudo apt-get install python-wstool
fi

# Update workspace with wstool
wstool update -t src $@
