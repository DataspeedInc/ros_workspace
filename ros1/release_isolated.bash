#!/bin/bash
if [ -f devel_isolated/setup.bash ]; then
	source devel_isolated/setup.bash
fi
catkin_make_isolated -DCMAKE_BUILD_TYPE=Release $@
