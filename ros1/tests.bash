#!/bin/bash
set -e

source devel/setup.bash
catkin_make -DCMAKE_BUILD_TYPE=Release
catkin_make -DCMAKE_BUILD_TYPE=Release tests
catkin_make -DCMAKE_BUILD_TYPE=Release run_tests
catkin_test_results build/test_results
