#!/bin/bash
set -e

# Check for rosdep
if ! [ $(which rosdep) ]; then
  echo "Installing rosdep..."
  echo -e "\033[1mExecuting command [sudo apt-get install python3-rosdep]\033[0m"
  sudo apt-get install python3-rosdep
  echo -e "\033[1mExecuting command [sudo rosdep init]\033[0m"
  sudo rosdep init
  echo -e "\033[1mExecuting command [rosdep update]\033[0m"
  rosdep update
fi

# Install dependencies with rosdep
rosdep install --from-paths src --ignore-src -r $@
