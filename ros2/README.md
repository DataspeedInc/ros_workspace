# ROS Workspace

Collection of scripts and documentation to automate using a ros2/colcon workspace. Most scripts must be run from the root of a colcon workspace.

## IDE setup

1. [Visual Studio Code](IDE/vscode.md)

## Setup/Update Scripts

1. [update](update.bash) (Update version controlled software with [rosinstall](http://docs.ros.org/independent/api/rosinstall/html/) and [wstool](http://wiki.ros.org/wstool))
1. [check_changes](check_changes.sh) (Check for changes in version controlled software with [rosinstall](http://docs.ros.org/independent/api/rosinstall/html/) and [wstool](http://wiki.ros.org/wstool))
1. [deps](deps.bash) (Install dependencies with [rosdep](http://docs.ros.org/independent/api/rosdep/html/))

## Build Scripts

1. [debug](debug.bash) (Build workspace in debug mode)
1. [release](release.bash) (Build workspace in release mode)
1. [tests](tests.bash) (Build workspace in release mode and run tests)
