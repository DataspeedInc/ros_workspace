#!/bin/bash
set -e

# Check for wstool
if ! [ $(which wstool) ]; then
  echo "Installing wstool..."
  echo -e "\033[1mExecuting command [sudo apt-get install python3-wstool]\033[0m"
  sudo apt-get install python3-wstool
fi

# Update workspace with wstool
wstool update -t src $@
