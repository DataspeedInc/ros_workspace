#!/bin/bash

# Check for colcon
if ! [ $(which colcon) ]; then
  echo "Installing colcon..."
  echo -e "\033[1mExecuting command [sudo apt-get install python3-colcon-common-extensions]\033[0m"
  sudo apt-get install python3-colcon-common-extensions
fi

# Build with colcon
source install/setup.bash
colcon build $@ --symlink-install --cmake-args -DCMAKE_BUILD_TYPE=Debug
